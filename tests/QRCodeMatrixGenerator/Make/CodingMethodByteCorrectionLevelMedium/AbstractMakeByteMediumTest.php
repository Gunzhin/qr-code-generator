<?php

namespace QRCodeGenerator\Tests\QRCodeMatrixGenerator\Make\CodingMethodByteCorrectionLevelMedium;

use QRCodeGenerator\QRCodeMatrix;
use QRCodeGenerator\Tests\QRCodeMatrixGenerator\Make\AbstractMakeTest;

abstract class AbstractMakeByteMediumTest extends AbstractMakeTest
{
    /**
     * @var string
     */
    protected $coding_method = QRCodeMatrix::CODING_METHOD_BYTE;

    /**
     * @var string
     */
    protected $correction_level = QRCodeMatrix::CORRECTION_LEVEL_MEDIUM;

    /**
     * @var array
     */
    protected $input_exploded_string;

    /**
     * @var array
     */
    protected $expected_matrix;
}