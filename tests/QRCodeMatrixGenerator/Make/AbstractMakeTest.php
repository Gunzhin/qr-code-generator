<?php

namespace QRCodeGenerator\Tests\QRCodeMatrixGenerator\Make;

use PHPUnit\Framework\TestCase;
use QRCodeGenerator\Configurations\DefaultConfiguration;
use QRCodeGenerator\QRCodeMatrixGenerator;

abstract class AbstractMakeTest extends TestCase
{
    /**
     * @var string
     */
    protected $coding_method;

    /**
     * @var string
     */
    protected $correction_level;

    /**
     * @var array
     */
    protected $input_exploded_string;

    /**
     * @var array
     */
    protected $expected_matrix;


    public function testMake(): void
    {
        $generator = new QRCodeMatrixGenerator();

        $input = implode('', $this->input_exploded_string);

        $matrix = $generator->make(
            new DefaultConfiguration(),
            $input
        );

        $this->assertEquals($this->expected_matrix, $matrix->content());
    }
}