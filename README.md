This is a prototype library for generating qr-code image.

---

Example of use:

```php
use QRCodeGenerator\QRCodeGenerator;

$generator = new QRCodeGenerator();
$generator->make('Random string', './', 'Example');
```

QR-code image will appear in the current folder.

---

_Detailed manual will be added later..._