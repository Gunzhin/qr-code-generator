<?php

namespace QRCodeGenerator;

use QRCodeGenerator\Builders\FinalBinaryStringBuilder;
use QRCodeGenerator\Builders\VersionBuilder;
use QRCodeGenerator\Contracts\QRCodeConfigurationContract;
use QRCodeGenerator\Contracts\QRCodeCorrectionLevelContract;
use QRCodeGenerator\Contracts\QRCodeMatrixContract;
use QRCodeGenerator\Contracts\QRCodeMatrixGeneratorContract;
use QRCodeGenerator\Exceptions\InvalidParametersException;
use QRCodeGenerator\Exceptions\NotSupportedException;

class QRCodeMatrixGenerator implements QRCodeMatrixGeneratorContract
{
    /**
     * @var VersionBuilder
     */
    protected VersionBuilder $versionBuilder;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->versionBuilder = new VersionBuilder();
    }

    /**
     * @param QRCodeConfigurationContract $configuration
     * @param string $content
     *
     * @return QRCodeMatrixContract
     *
     * @throws InvalidParametersException
     * @throws NotSupportedException
     */
    public function make(
        QRCodeConfigurationContract $configuration,
        string $content
    ): QRCodeMatrixContract
    {
        $codingMethod = $configuration->codingMethod();
        $correctionLevel = $configuration->correctionLevel();
        $minimalVersion = $configuration->minimalVersion();

        $payload_binary_data = $codingMethod->makePayloadBinaryData($content);

        $version = $this->versionBuilder->getVersion($correctionLevel, $payload_binary_data, $minimalVersion);

        $full_binary_data = $codingMethod->makeFullBinaryData($payload_binary_data, $version);

        $builder = new FinalBinaryStringBuilder();

        $result_binary_string = $builder->make($correctionLevel, $full_binary_data, $version);

        $matrix = $this->makeMatrixContent($correctionLevel, $result_binary_string, $version);

        return new QRCodeMatrix(
            $content,
            $codingMethod->getCodeName(),
            $correctionLevel->getCodeName(),
            $version,
            $matrix
        );
    }

    protected function makeMatrixContent(QRCodeCorrectionLevelContract $correctionLevel, string $binary_string, int $version): array
    {
        $qr_code_size = 17 + ($version * 4) + 8;

        $leveling_pixels_centers = $this->versionBuilder->getPixelCentersBuVersions()[$version];

        $content = [];

        for ($n = 0; $n < $qr_code_size; $n++) {
            $content[$n] = array_fill(0, $qr_code_size, -1);
        }

        for ($x = 0; $x < $qr_code_size; $x++) {
            for ($y = 0; $y < 4; $y++) {
                $content[$x][$y] = 0;
                $content[$x][$qr_code_size - $y - 1] = 0;
                $content[$y][$x] = 0;
                $content[$qr_code_size - $y - 1][$x] = 0;
            }
        }

        $search_squares = [
            [4, 4],
            [4, $qr_code_size - 11],
            [$qr_code_size - 11, 4],
        ];

        foreach ($search_squares as $coordinates) {
            $start_x = $coordinates[0];
            $start_y = $coordinates[1];

            for ($n = 0; $n < 7; $n++) {
                $content[$start_x + $n][$start_y + 0] = 1;
                $content[$start_x + $n][$start_y + 6] = 1;
                $content[$start_x + 0][$start_y + $n] = 1;
                $content[$start_x + 6][$start_y + $n] = 1;
            }

            for ($n = -1; $n < 8; $n++) {
                $content[$start_x + $n][$start_y - 1] = 0;
                $content[$start_x + $n][$start_y + 7] = 0;
                $content[$start_x - 1][$start_y + $n] = 0;
                $content[$start_x + 7][$start_y + $n] = 0;
            }

            for ($n = 1; $n < 6; $n++) {
                $content[$start_x + $n][$start_y + 1] = 0;
                $content[$start_x + $n][$start_y + 5] = 0;
                $content[$start_x + 1][$start_y + $n] = 0;
                $content[$start_x + 5][$start_y + $n] = 0;
            }

            for ($n = 0; $n < 3; $n++) {
                for ($i = 0; $i < 3; $i++) {
                    $content[$start_x + $n + 2][$start_y + $i + 2] = 1;
                }
            }
        }

        $sync_line_start = [12, 10];

        for ($n = 0; $n < $qr_code_size - 22; $n++) {
            $x = $n + $sync_line_start[0];
            $y = $sync_line_start[1];

            $content[$x][$y] = ($n + 1) % 2;
            $content[$y][$x] = ($n + 1) % 2;
        }

        if (! empty($leveling_pixels_centers)) {
            foreach ($leveling_pixels_centers as $key_x => $leveling_x) {
                $leveling_x += 4;
                foreach ($leveling_pixels_centers as $key_y => $leveling_y) {
                    $leveling_y += 4;

                    if (
                        $leveling_x + $leveling_y > 20 &&
                        abs($leveling_x - $leveling_y) + 21 !== $qr_code_size
                    ) {
                        for ($n = -2; $n < 3; $n++) {
                            $content[$leveling_x + $n][$leveling_y - 2] = 1;
                            $content[$leveling_x + $n][$leveling_y + 2] = 1;
                            $content[$leveling_x - 2][$leveling_y + $n] = 1;
                            $content[$leveling_x + 2][$leveling_y + $n] = 1;
                        }

                        for ($n = -1; $n < 2; $n++) {
                            $content[$leveling_x + $n][$leveling_y - 1] = 0;
                            $content[$leveling_x + $n][$leveling_y + 0] = 0;
                            $content[$leveling_x + $n][$leveling_y + 1] = 0;
                        }

                        $content[$leveling_x][$leveling_y] = 1;
                    }
                }
            }
        }

        $offset_x = 4;
        $offset_y = $qr_code_size - 15;

        if ($version >= 7) {
            $version_matrix = $this->versionBuilder->getVersionMatrix()[$version];

            foreach ($version_matrix as $y => $line_x) {
                foreach ($line_x as $x => $value) {
                    $content[$x + $offset_x][$y + $offset_y] = $value;
                    $content[$y + $offset_y][$x + $offset_x] = $value;
                }
            }
        }

        $mask_correction_code = $correctionLevel->getMaskCorrectionCodes();

        for ($n = 0; $n < 7; $n++) {
            $x1 = $n + 4;
            $y1 = 12;

            $x2 = 12;
            $y2 = $qr_code_size - $x1 - 1;

            if ($n === 6) {
                $x1++;
                $content[$x2][$y2 - 1] = 1;
            }

            $content[$x1][$y1] = $mask_correction_code[$n];
            $content[$x2][$y2] = $mask_correction_code[$n];
        }

        for ($n = 0; $n < 8; $n++) {
            $x1 = 12;
            $y1 = 12 - $n;

            $x2 = $qr_code_size - $y1;
            $y2 = 12;

            if ($n >= 2) {
                $y1--;
            }

            $content[$x1][$y1] = $mask_correction_code[$n + 7];
            $content[$x2][$y2] = $mask_correction_code[$n + 7];
        }

        $x = $qr_code_size - 5;
        $y = $qr_code_size - 5;
        $dx = -1;
        $dy = -1;
        $dp = -1;

        $content_order = [
            [$x, $y]
        ];

        while ($x > 3) {
            if ($dp > 0) {
                $y += $dy;
            }

            $x += $dx;

            $dx *= -1;
            $dp *= -1;

            if (
                $y < 4 ||
                $y > $qr_code_size - 4
            ) {
                $dy *= -1;

                $y += $dy;
                $x -= ($x === 12) ? 3 : 2;
            }

            $value = $content[$x][$y] ?? 0;

            if ($value < 0) {
                $content_order[] = [$x, $y];
            }
        }

        for ($n = 0; $n < strlen($binary_string); $n++) {
            $origin_value = (int) $binary_string[$n];

            if (array_key_exists($n, $content_order)) {
                [$x, $y] = $content_order[$n];

                $mask_value = ($x - 4) % 3 === 0
                    ? (int) !$origin_value
                    : $origin_value;

                $content[$x][$y] = $mask_value;
            }
        }

        return $content;
    }
}
