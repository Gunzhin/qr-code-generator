<?php

namespace QRCodeGenerator;

use QRCodeGenerator\Configurations\DefaultConfiguration;
use QRCodeGenerator\Contracts\QRCodeConfigurationContract;
use QRCodeGenerator\Contracts\QRCodeGeneratorContract;
use QRCodeGenerator\Contracts\QRCodeImageCreatorContract;
use QRCodeGenerator\Exceptions\InvalidParametersException;
use QRCodeGenerator\Exceptions\NotSupportedException;
use QRCodeGenerator\ImageCreators\GDPngImageCreator;

class QRCodeGenerator implements QRCodeGeneratorContract
{
    /**
     * @var QRCodeImageCreatorContract|null
     */
    protected $imageCreator;

    /**
     * @var QRCodeConfigurationContract|null
     */
    protected $configuration;

    /**
     * @param string $content
     * @param string $dir
     * @param string $name
     *
     * @return bool
     *
     * @throws InvalidParametersException
     * @throws NotSupportedException
     */
    public function make(string $content, string $dir, string $name): bool
    {
        $configuration = $this->configuration ?? new DefaultConfiguration();
        $imageCreator = $this->imageCreator ?? new GDPngImageCreator();

        $matrix = (new QRCodeMatrixGenerator())->make($configuration, $content);

        return $imageCreator->make($matrix, $dir, $name);
    }

    /**
     * @param QRCodeImageCreatorContract $imageCreator
     *
     * @return QRCodeGeneratorContract
     */
    public function setImageCreator(QRCodeImageCreatorContract $imageCreator): QRCodeGeneratorContract
    {
        $this->imageCreator = $imageCreator;

        return $this;
    }

    /**
     * @param QRCodeConfigurationContract $configuration
     *
     * @return QRCodeGeneratorContract
     */
    public function setConfiguration(QRCodeConfigurationContract $configuration): QRCodeGeneratorContract
    {
        $this->configuration = $configuration;

        return $this;
    }
}
