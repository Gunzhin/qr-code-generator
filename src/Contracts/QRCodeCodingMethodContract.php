<?php

namespace QRCodeGenerator\Contracts;

interface QRCodeCodingMethodContract
{
    /**
     * @return string
     */
    public function getCodeName(): string;

    /**
     * @param string $content
     *
     * @return string
     */
    public function makePayloadBinaryData(string $content): string;

    /**
     * @param string $payload
     * @param int $version
     *
     * @return string
     */
    public function makeFullBinaryData(string $payload, int $version): string;
}