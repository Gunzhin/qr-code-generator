<?php

namespace QRCodeGenerator\Contracts;

interface QRCodeGeneratorContract
{
    /**
     * @param string $content
     * @param string $dir
     * @param string $name
     *
     * @return bool
     */
    public function make(string $content, string $dir, string $name): bool;

    /**
     * @param QRCodeImageCreatorContract $imageCreator
     *
     * @return $this
     */
    public function setImageCreator(QRCodeImageCreatorContract $imageCreator): self;

    /**
     * @param QRCodeConfigurationContract $configuration
     *
     * @return $this
     */
    public function setConfiguration(QRCodeConfigurationContract $configuration): self;
}
