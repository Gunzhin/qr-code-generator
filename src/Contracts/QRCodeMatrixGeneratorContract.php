<?php

namespace QRCodeGenerator\Contracts;

interface QRCodeMatrixGeneratorContract
{
    /**
     * @param QRCodeConfigurationContract $configuration
     * @param string $content
     *
     * @return QRCodeMatrixContract
     */
    public function make(QRCodeConfigurationContract $configuration, string $content): QRCodeMatrixContract;
}
