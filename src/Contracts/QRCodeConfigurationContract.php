<?php

namespace QRCodeGenerator\Contracts;

interface QRCodeConfigurationContract
{
    /**
     * @return QRCodeCodingMethodContract
     */
    public function codingMethod(): QRCodeCodingMethodContract;

    /**
     * @return QRCodeCorrectionLevelContract
     */
    public function correctionLevel(): QRCodeCorrectionLevelContract;

    /**
     * @return int
     */
    public function minimalVersion(): int;
}
