<?php

namespace QRCodeGenerator\Contracts;

interface QRCodeCorrectionLevelContract
{
    /**
     * @return string
     */
    public function getCodeName(): string;

    /**
     * @return int[]
     */
    public function getBlocksCountByVersions(): array;

    /**
     * @return int[]
     */
    public function getCorrectionBytesNumbersByVersions(): array;

    /**
     * @return int[]
     */
    public function getVersionSizes(): array;

    /**
     * @return int[]
     */
    public function getMaskCorrectionCodes(): array;
}