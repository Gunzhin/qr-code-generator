<?php

namespace QRCodeGenerator\Contracts;

interface QRCodeImageCreatorContract
{
    /**
     * @param QRCodeMatrixContract $matrix
     * @param string $dir
     * @param string $name
     *
     * @return bool
     */
    public function make(QRCodeMatrixContract $matrix, string $dir, string $name): bool;
}
