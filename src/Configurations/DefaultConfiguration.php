<?php

namespace QRCodeGenerator\Configurations;

use QRCodeGenerator\Builders\CodingMethods\ByteCodingMethod;
use QRCodeGenerator\Builders\CorrectionLevels\MediumCorrectionLevel;

/**
 * Class DefaultConfiguration
 *
 * @package QRCodeGenerator\Configurations
 */
class DefaultConfiguration extends Configuration
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->codingMethod = new ByteCodingMethod();
        $this->correctionLevel = new MediumCorrectionLevel();
        $this->minimalVersion = 0;
    }
}
