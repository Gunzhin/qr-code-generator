<?php

namespace QRCodeGenerator\Configurations;

use QRCodeGenerator\Contracts\QRCodeCodingMethodContract;
use QRCodeGenerator\Contracts\QRCodeConfigurationContract;
use QRCodeGenerator\Contracts\QRCodeCorrectionLevelContract;

/**
 * Abstract class Configuration
 *
 * @package QRCodeGenerator\Configurations
 */
abstract class Configuration implements QRCodeConfigurationContract
{
    /**
     * @var QRCodeCodingMethodContract
     */
    protected QRCodeCodingMethodContract $codingMethod;

    /**
     * @var QRCodeCorrectionLevelContract
     */
    protected QRCodeCorrectionLevelContract $correctionLevel;

    /**
     * @var int
     */
    protected int $minimalVersion;

    /**
     * @return QRCodeCodingMethodContract
     */
    public function codingMethod(): QRCodeCodingMethodContract
    {
        return $this->codingMethod;
    }

    /**
     * @return QRCodeCorrectionLevelContract
     */
    public function correctionLevel(): QRCodeCorrectionLevelContract
    {
        return $this->correctionLevel;
    }

    /**
     * @return int
     */
    public function minimalVersion(): int
    {
        return $this->minimalVersion;
    }
}
