<?php

namespace QRCodeGenerator\Builders\CodingMethods;

use QRCodeGenerator\Contracts\QRCodeCodingMethodContract;
use QRCodeGenerator\Contracts\QRCodeMatrixContract;

class ByteCodingMethod implements QRCodeCodingMethodContract
{
    /**
     * @return string
     */
    public function getCodeName(): string
    {
        return QRCodeMatrixContract::CODING_METHOD_BYTE;
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public function makePayloadBinaryData(string $content): string
    {
        $b_origin = '';

        for ($n = 0; $n < strlen($content); $n++) {
            $b_origin .= str_pad(decbin(ord($content[$n])), 8, '0', STR_PAD_LEFT);
        }

        return $b_origin;
    }

    /**
     * @param string $payload
     * @param int $version
     *
     * @return string
     */
    public function makeFullBinaryData(string $payload, int $version): string
    {
        $count_length = $version >= 10 ? 16 : 8;

        $b_type = '0100';
        $b_count = str_pad(decbin(strlen($payload) / 8), $count_length, '0', STR_PAD_LEFT);

        return $b_type . $b_count . $payload;
    }
}