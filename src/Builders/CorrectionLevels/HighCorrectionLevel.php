<?php

namespace QRCodeGenerator\Builders\CorrectionLevels;

use QRCodeGenerator\Contracts\QRCodeCorrectionLevelContract;
use QRCodeGenerator\Contracts\QRCodeMatrixContract;

class HighCorrectionLevel implements QRCodeCorrectionLevelContract
{
    /**
     * @return string
     */
    public function getCodeName(): string
    {
        return QRCodeMatrixContract::CORRECTION_LEVEL_HIGH;
    }

    /**
     * @return int[]
     */
    public function getBlocksCountByVersions(): array
    {
        return [
            1 => 1,
            2 => 1,
            3 => 2,
            4 => 4,
            5 => 4,
            6 => 4,
            7 => 5,
            8 => 6,
            9 => 8,
            10 => 8,
            11 => 11,
            12 => 11,
            13 => 16,
            14 => 16,
            15 => 18,
            16 => 16,
            17 => 19,
            18 => 21,
            19 => 25,
            20 => 25,
            21 => 25,
            22 => 34,
            23 => 30,
            24 => 32,
            25 => 35,
            26 => 37,
            27 => 40,
            28 => 42,
            29 => 45,
            30 => 48,
            31 => 51,
            32 => 54,
            33 => 57,
            34 => 60,
            35 => 63,
            36 => 66,
            37 => 70,
            38 => 74,
            39 => 77,
            40 => 81,
        ];
    }

    /**
     * @return int[]
     */
    public function getCorrectionBytesNumbersByVersions(): array
    {
        return [
            1 => 17, 2 => 28, 3 => 22, 4 => 16, 5 => 22, 6 => 28, 7 => 26, 8 => 26, 9 => 24, 10 => 28,
            11 => 24, 12 => 28, 13 => 22, 14 => 24, 15 => 24, 16 => 30, 17 => 28, 18 => 28, 19 => 26, 20 => 28,
            21 => 30, 22 => 24, 23 => 30, 24 => 30, 25 => 30, 26 => 30, 27 => 30, 28 => 30, 29 => 30, 30 => 30,
            31 => 30, 32 => 30, 33 => 30, 34 => 30, 35 => 30, 36 => 30, 37 => 30, 38 => 30, 39 => 30, 40 => 30,
        ];
    }


    /**
     * @return int[]
     */
    public function getVersionSizes(): array
    {
        return [
            1 => 72, 2 => 128, 3 => 208, 4 => 288, 5 => 368,
            6 => 480, 7 => 528, 8 => 688, 9 => 800, 10 => 976,
            11 => 1120, 12 => 1264, 13 => 1440, 14 => 1576, 15 => 1784,
            16 => 2024, 17 => 2264, 18 => 2504, 19 => 2728, 20 => 3080,
            21 => 3248, 22 => 3536, 23 => 3712, 24 => 4112, 25 => 4304,
            26 => 4768, 27 => 5024, 28 => 5288, 29 => 5608, 30 => 5960,
            31 => 6344, 32 => 6760, 33 => 7208, 34 => 7688, 35 => 7888,
            36 => 8432, 37 => 8768, 38 => 9136, 39 => 9776, 40 => 10208,
        ];
    }

    /**
     * @return int[]
     */
    public function getMaskCorrectionCodes(): array
    {
        return [0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1];
    }
}