<?php

namespace QRCodeGenerator\Builders\CorrectionLevels;

use QRCodeGenerator\Contracts\QRCodeCorrectionLevelContract;
use QRCodeGenerator\Contracts\QRCodeMatrixContract;

class MediumCorrectionLevel implements QRCodeCorrectionLevelContract
{
    /**
     * @return string
     */
    public function getCodeName(): string
    {
        return QRCodeMatrixContract::CORRECTION_LEVEL_MEDIUM;
    }

    /**
     * @return int[]
     */
    public function getBlocksCountByVersions(): array
    {
        return [
            1 => 1,
            2 => 1,
            3 => 1,
            4 => 2,
            5 => 2,
            6 => 4,
            7 => 4,
            8 => 4,
            9 => 5,
            10 => 5,
            11 => 5,
            12 => 8,
            13 => 9,
            14 => 9,
            15 => 10,
            16 => 10,
            17 => 11,
            18 => 13,
            19 => 14,
            20 => 16,
            21 => 17,
            22 => 17,
            23 => 18,
            24 => 20,
            25 => 21,
            26 => 23,
            27 => 25,
            28 => 26,
            29 => 28,
            30 => 29,
            31 => 31,
            32 => 33,
            33 => 35,
            34 => 37,
            35 => 38,
            36 => 40,
            37 => 43,
            38 => 45,
            39 => 47,
            40 => 49,
        ];
    }

    /**
     * @return int[]
     */
    public function getCorrectionBytesNumbersByVersions(): array
    {
        return [
            1 => 10, 2 => 16, 3 => 26, 4 => 18, 5 => 24, 6 => 16, 7 => 18, 8 => 22, 9 => 22, 10 => 26,
            11 => 30, 12 => 22, 13 => 22, 14 => 24, 15 => 24, 16 => 28, 17 => 28, 18 => 26, 19 => 26, 20 => 26,
            21 => 26, 22 => 28, 23 => 28, 24 => 28, 25 => 28, 26 => 28, 27 => 28, 28 => 28, 29 => 28, 30 => 28,
            31 => 28, 32 => 28, 33 => 28, 34 => 28, 35 => 28, 36 => 28, 37 => 28, 38 => 28, 39 => 28, 40 => 28,
        ];
    }

    /**
     * @return int[]
     */
    public function getVersionSizes(): array
    {
        return [
            1 => 128, 2 => 224, 3 => 352, 4 => 512, 5 => 688,
            6 => 864, 7 => 992, 8 => 1232, 9 => 1456, 10 => 1728,
            11 => 2032, 12 => 2320, 13 => 2672, 14 => 2920, 15 => 3320,
            16 => 3624, 17 => 4056, 18 => 4504, 19 => 5016, 20 => 5352,
            21 => 5712, 22 => 6256, 23 => 6880, 24 => 7312, 25 => 8000,
            26 => 8496, 27 => 9024, 28 => 9544, 29 => 10136, 30 => 10984,
            31 => 11640, 32 => 12328, 33 => 13048, 34 => 13800, 35 => 14496,
            36 => 15312, 37 => 15936, 38 => 16816, 39 => 17728, 40 => 18672,
        ];
    }

    /**
     * @return int[]
     */
    public function getMaskCorrectionCodes(): array
    {
        return [1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0];
    }
}