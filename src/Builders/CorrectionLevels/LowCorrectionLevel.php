<?php

namespace QRCodeGenerator\Builders\CorrectionLevels;

use QRCodeGenerator\Contracts\QRCodeCorrectionLevelContract;
use QRCodeGenerator\Contracts\QRCodeMatrixContract;

class LowCorrectionLevel implements QRCodeCorrectionLevelContract
{
    /**
     * @return string
     */
    public function getCodeName(): string
    {
        return QRCodeMatrixContract::CORRECTION_LEVEL_LOW;
    }

    /**
     * @return int[]
     */
    public function getBlocksCountByVersions(): array
    {
        return [
            1 => 1,
            2 => 1,
            3 => 1,
            4 => 1,
            5 => 1,
            6 => 2,
            7 => 2,
            8 => 2,
            9 => 2,
            10 => 4,
            11 => 4,
            12 => 4,
            13 => 4,
            14 => 4,
            15 => 6,
            16 => 6,
            17 => 6,
            18 => 6,
            19 => 7,
            20 => 8,
            21 => 8,
            22 => 9,
            23 => 9,
            24 => 10,
            25 => 12,
            26 => 12,
            27 => 12,
            28 => 13,
            29 => 14,
            30 => 15,
            31 => 16,
            32 => 17,
            33 => 18,
            34 => 19,
            35 => 19,
            36 => 20,
            37 => 21,
            38 => 22,
            39 => 24,
            40 => 25,
        ];
    }

    /**
     * @return int[]
     */
    public function getCorrectionBytesNumbersByVersions(): array
    {
        return [
            1 => 7, 2 => 10, 3 => 15, 4 => 20, 5 => 26, 6 => 18, 7 => 20, 8 => 24, 9 => 30, 10 => 18,
            11 => 20, 12 => 24, 13 => 26, 14 => 30, 15 => 22, 16 => 24, 17 => 28, 18 => 30, 19 => 28, 20 => 28,
            21 => 28, 22 => 28, 23 => 30, 24 => 30, 25 => 26, 26 => 28, 27 => 30, 28 => 30, 29 => 30, 30 => 30,
            31 => 30, 32 => 30, 33 => 30, 34 => 30, 35 => 30, 36 => 30, 37 => 30, 38 => 30, 39 => 30, 40 => 30,
        ];
    }

    /**
     * @return int[]
     */
    public function getVersionSizes(): array
    {
        return [
            1 => 152, 2 => 272, 3 => 440, 4 => 640, 5 => 864,
            6 => 1088, 7 => 1248, 8 => 1552, 9 => 1856, 10 => 2192,
            11 => 2592, 12 => 2960, 13 => 3424, 14 => 3688, 15 => 4184,
            16 => 4712, 17 => 5176, 18 => 5768, 19 => 6360, 20 => 6888,
            21 => 7456, 22 => 8048, 23 => 8752, 24 => 9392, 25 => 10208,
            26 => 10960, 27 => 11744, 28 => 12248, 29 => 13048, 30 => 13880,
            31 => 14744, 32 => 15640, 33 => 16568, 34 => 17528, 35 => 18448,
            36 => 19472, 37 => 20528, 38 => 21616, 39 => 22496, 40 => 23648,
        ];
    }

    /**
     * @return int[]
     */
    public function getMaskCorrectionCodes(): array
    {
        return [1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0];
    }
}