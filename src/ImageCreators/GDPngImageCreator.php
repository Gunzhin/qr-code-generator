<?php

namespace QRCodeGenerator\ImageCreators;

use QRCodeGenerator\Contracts\QRCodeImageCreatorContract;
use QRCodeGenerator\Contracts\QRCodeMatrixContract;

/**
 * Class GDPngImageCreator
 *
 * @package QRCodeGenerator\ImageCreators
 */
class GDPngImageCreator implements QRCodeImageCreatorContract
{
    public function make(QRCodeMatrixContract $matrix, string $dir, string $name): bool
    {
        $content = $matrix->content();

        $square = function ($image, int $x, int $y, int $v, int $s = 1) {
            $x *= $s;
            $y *= $s;

            $square = [$x, $y, $x + $s, $y, $x + $s, $y + $s, $x, $y + $s];

            if ($v === 1) {
                $color = imagecolorallocate($image, 20, 20, 20);
            } else {
                $color = imagecolorallocate($image, 240, 240, 240);
            }

            imagefilledpolygon($image, $square, 4, $color);
        };

        $pixel_size = 8;
        $qr_code_size = $matrix->version() * 4 + 25;

        $image = imagecreatetruecolor($qr_code_size * $pixel_size, $qr_code_size * $pixel_size);

        foreach ($content as $x => $line) {
            foreach ($line as $y => $value) {
                $square($image, $x, $y, $value, $pixel_size);
            }
        }

        $path = str_replace('//', '/', $dir . '/' . $name . '.png');

        $file = fopen($path, 'wb');

        imagepng($image, $file);
        imagedestroy($image);

        fclose($file);

        return file_exists($path);
    }
}
